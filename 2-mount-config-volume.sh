#!/usr/bin/env bash

docker run -d --name gitlab-runner --restart always \
    -v "$PWD/rnnr/config":/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$PWD:$PWD" \
    gitlab/gitlab-runner:latest
