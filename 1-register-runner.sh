#!/usr/bin/env bash

. "$PWD"/rnnr/0-variables.sh

docker run --rm -it \
    -v "$PWD/rnnr/config":/etc/gitlab-runner \
    gitlab/gitlab-runner:latest \
    register \
    --non-interactive \
    --executor "docker" \
    --docker-image "$IMAGE" \
    --url "https://gitlab.com/" \
    --registration-token "$TOKEN" \
    --description "docker-runner" \
    --tag-list "$TAGS" \
    --run-untagged \
    --locked="false"
