# rnnr

## Adding **rnnr** source code to a rnnr directory of an arbitrary repo
*Before* running the following command, ensure you are not tracking the 'rnnr'
directory you are about to create in your repo by adding, e.g., `rnnr/**` to
your `.gitignore` file. The **rnnr** utilities are intended to support
development/debugging. As such, they do not need to be a component of your
repo's remote.
```sh
git clone --depth=1 https://gitlab.com/lzachmann/rnnr.git rnnr \
  && rm -rf rnnr/.git \
  && rm rnnr/.gitignore rnnr/README.md rnnr/LICENSE
```

Once you've obtained a copy of the **rnnr** shell scripts, update
'rnnr/0-variables.sh', then run the next two scripts to register the Runner and
mount the config volume to that specific Runner. This completes the setup
required to use GitLab Runner locally. Stages of the pipeline can be tested
interactively (as described below), or by invoking
`./rnnr/3-run-test.sh <job-name>`.

## Interactive work
```sh
docker exec -it -w "$PWD" gitlab-runner bash
```

### Debugging
Once in the container with the above command, run:
```sh
gitlab-runner --debug exec docker <job-name>
```

## Other resources
- Commands: <https://gitlab.com/gitlab-org/gitlab-runner/blob/master/docs/commands/README.md#gitlab-runner-exec>
Note: this set of shell scripts was inspired by the discussion here: <https://gitlab.com/gitlab-org/gitlab-runner/issues/2438#note_34431388>.
