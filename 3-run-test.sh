#!/usr/bin/env bash

docker exec -it -w "$PWD" gitlab-runner \
    gitlab-runner exec docker "$1"
